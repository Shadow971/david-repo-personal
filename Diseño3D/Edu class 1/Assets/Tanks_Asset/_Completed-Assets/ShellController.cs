﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellController : MonoBehaviour
{
    float time = 3f;
    public float speed;
    public float gravity;

    public float currentYSpeed;
    void Update()
    {
        float dt = Time.deltaTime;
        Destroy(dt);
        Movement(dt);

    }
    private void Destroy (float dt)
    {
        if (time <= 0f)
            Destroy(this.gameObject);
        else
            time -= Time.deltaTime;
    }
    private void Movement(float dt)
    {
        ///Movement Z
        transform.position += transform.forward * speed * dt;
        
        ///Movement Y
        float offset = gravity - currentYSpeed;
        offset = Mathf.Clamp(offset, -gravity * dt, gravity * dt);
        currentYSpeed += offset;

        transform.position += -transform.up * currentYSpeed * dt;
    }

}