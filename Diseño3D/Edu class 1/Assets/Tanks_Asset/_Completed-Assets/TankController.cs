﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    [Header ("Movement")]
    public float maxZSpeed = 5f;
    public float currentZSpeed;
    public float maxAccel;

    [Header ("Rotation")]
    public float maxRotateSpeed = 5f;
    public float currentRotateSpeed;
    public float maxRotateAccel;
    public float turretSpeed;

    [Header ("GameObjects")]
    public GameObject turret;
    public GameObject body;
    public GameObject shoot;
    public GameObject shell;

    [Header("Time")]
    public float reloadTime;
    public float currentTime = 0f;


    void Update()
    {
        float dt = Time.deltaTime;
        currentTime -= dt;

        Vector2 movInputs;
        movInputs.x = Input.GetAxis("Horizontal");
        movInputs.y = Input.GetAxis("Vertical");

        Vector2 movRotate;
        movRotate.x = Input.GetAxisRaw("Mouse X");
        movRotate.y = 0;
        RotateTurret(dt, movRotate);


        if ((Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) && currentTime <= 0)
        {
            currentTime = reloadTime;
            Instantiate(shell, shoot.transform.position, shoot.transform.rotation);
        }
           
        Move(dt, movInputs);
        RotateBody(dt, movInputs);
    }

    private void Move(float dt, Vector2 movInputs)
    {
        float targetSpeed = movInputs.y * maxZSpeed;
        float offset = targetSpeed - currentZSpeed;
        offset = Mathf.Clamp(offset, -maxAccel * dt, maxAccel * dt);
        currentZSpeed += offset;

        body.transform.position += transform.forward * currentZSpeed * dt;
    }

    private void RotateBody(float dt, Vector2 movInputs)
    {
        float targetRotateSpeed = movInputs.x * maxRotateSpeed;
        float rotateOffset = targetRotateSpeed - currentRotateSpeed;
        rotateOffset = Mathf.Clamp(rotateOffset, -maxRotateAccel * dt, maxRotateAccel * dt);
        currentRotateSpeed += rotateOffset;

        body.transform.Rotate(Vector3.up * currentRotateSpeed);
    }

    private void RotateTurret(float dt, Vector2 movRotate)
    {
        turret.transform.Rotate(Vector3.up * turretSpeed * dt * movRotate.x * 10);
    }
}