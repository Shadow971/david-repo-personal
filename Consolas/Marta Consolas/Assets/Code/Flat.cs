﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flat : MonoBehaviour
{
    Vector3 rightVector;

    public Transform[] platforms;

    public void Start()
    {
        rightVector = new Vector3(2.4f, 0, 0);
    }

    public int HorizontalMove(Transform currentPlatform)
    {
        int platformsLeft = 0;
        int platformsRight = 0;
        bool right = false;

        for (int i = 0; i < platforms.Length; i++)
        {
            if(platforms[i].name == currentPlatform.name)
            {
                right = true;
            }
            else
            {
                if(right)
                {
                    platformsRight++;
                }
                else
                {
                    platformsLeft++;
                }
            }
        }

        Debug.Log(platformsRight + " " + platformsLeft);

        if (platformsRight > platformsLeft)
        {
            return 0;
        }
        if(platformsRight < platformsLeft)
        {
            return 1;
        }
        return -1;
    }

    public void HorizontalMovement(int dir)
    {
        Transform leftPlatform = null;
        Transform rightPlatform = null;

        if (dir == 1)
        {
            leftPlatform = platforms[0];
            rightPlatform = platforms[platforms.Length - 1];
            leftPlatform.position = rightPlatform.position + rightVector;
            ReOrder(true);

        }
        if (dir == 0)
        {
            leftPlatform = platforms[0];
            rightPlatform = platforms[platforms.Length - 1];
            rightPlatform.position = leftPlatform.position - rightVector;
            ReOrder(false);
        }
    }

    public void Pattern(Flat lastFlat)
    {
        int option;
        Random.InitState((int)Time.time);

        for (int i = 0; i < platforms.Length; i++)
        {
            platforms[i].gameObject.SetActive(false);
            if(lastFlat.platforms[i].gameObject.activeInHierarchy)
            {
                option = Random.Range(0, 3);

                if(i == 0)
                {
                    option = 0;
                }
                    
                switch(option)
                {
                    case 0:
                        platforms[i].gameObject.SetActive(true);
                        break;

                    case 1:
                        platforms[i].gameObject.SetActive(true);
                        break;

                    case 2:
                        platforms[i - 1].gameObject.SetActive(true);
                        break;

                    case 3:
                        platforms[i].gameObject.SetActive(true);
                        platforms[i - 1].gameObject.SetActive(true);
                        break;
                }
            }
            else
            {
                if(Random.Range(0,2) == 0)
                {
                    platforms[i].gameObject.SetActive(true);
                }
            }
        }
    }

    private void ReOrder(bool right)
    {
        Transform[] temporaryArray = new Transform [platforms.Length];
        int newPos;

        if (right)
        {
            for (int i = 0; i < platforms.Length; i++)
            {
                if (i == 0)
                    newPos = platforms.Length - 1;
                else
                    newPos = (i - 1);

                temporaryArray[newPos] = platforms[i];
            }
        }
        else
        {
            for (int i = 0; i < platforms.Length; i++)
            {
                if (i == platforms.Length - 1)
                    newPos = 0;
                else
                    newPos = (i + 1);

                temporaryArray[newPos] = platforms[i];
            }
        }

        for(int i=0; i < temporaryArray.Length; i++)
        {
            platforms[i] = temporaryArray[i];
        }
        
    }
}
