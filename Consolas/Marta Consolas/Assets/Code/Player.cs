﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float speed;

    public GameObject image;

    private Vector3 obj1Position;
    public Vector3 obj2Position;
    Transform obj = null;

    public bool isMoving = false;
    bool firstMovement = false;

    public bool rightButton = false;
    public bool leftButton = false;

    Transform platform;

    public int currentFlat;

    private float dt;
    private bool die;

    //singleton
    public static Player Instance;
    private void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(!die)
        {
            dt = Time.deltaTime;

            if (!isMoving)  //No se tiene que mover, esperando inputs
            {
                Inputs();
            }
            else //Movimiento 
            {
                Movement();
            }
        }
        
    }

    private void Movement() //Elegir q movimiento toca
    {
        if (!firstMovement)
        {
            FirstMovement();
        }
        else
        {
            if (obj2Position == Vector3.zero)
                Die();
            else
                SecondMovement();
        }
    }

    private void SecondMovement() //del intercambiador la plataforma
    {
        Vector3 dis = obj2Position - transform.position;
        if (dis.sqrMagnitude >= 0.01f)
        {
            transform.Translate(dis * speed * dt);
        }
        else
        {
            isMoving = false;
            firstMovement = false;
            obj = null;
            FlatsGenerator.Instance.PlatformHorizontalMovement(platform);
            FlatsGenerator.Instance.PlatformVerticalMovement();
            rightButton = false;
            leftButton = false;
            obj2Position = Vector3.zero;
            platform = null;
        }
    }

    private void FirstMovement() //de la plataforma al intercambiador
    {  
        Vector3 dis = (obj1Position - transform.position);
        if (dis.sqrMagnitude > 0.01f)
        {
            transform.Translate(dis * speed * dt);
        }
        else
        {
            firstMovement = true;
        }
    }

    private void Inputs() //recibe los inputs del player 
    {
        if (Input.GetKey(KeyCode.RightArrow) || rightButton)
        {
            rightButton = true;
            isMoving = true;
            obj = transform.GetChild(0);
            image.GetComponent<SpriteRenderer>().flipX = true;

        }
        if (Input.GetKey(KeyCode.LeftArrow) || leftButton)
        {
            leftButton = true;
            isMoving = true;
            obj = transform.GetChild(1);

            image.GetComponent<SpriteRenderer>().flipX = false;
        }
        if (obj != null && obj != null)
        {
            obj1Position = obj.position;
        }
    }

    public void RightButton()
    {
        rightButton = true;
    }
    public void LeftButton()
    {
        leftButton = true;
    }

    public void Die()
    {
        UIManager.Instance.Die();
        die = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Platform") && isMoving)
        {
            platform = other.transform;
            obj2Position = other.transform.position;
            obj2Position.y += 0.3f;
        }
        if(other.CompareTag("Cloud"))
        {
            Die();
        }
        if(other.CompareTag("Item"))
        {
            UIManager.Instance.ItemCollected();
            other.gameObject.SetActive(false);
            StartCoroutine(ObjAwake(other.gameObject));
        }
    }

    IEnumerator ObjAwake(GameObject obj)
    {
        yield return new WaitForSeconds(5);
        obj.SetActive(true);
    }
}
