﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject text;
    public GameObject exitButton;
    public GameObject replayButton;

    public GameObject rightButton;
    public GameObject leftButton;

    public GameObject timeBar;
    private float newScale = 1;
    private bool die = false;

    #region Sinlgeton
    public static UIManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion 

    private void Update()
    {
        if(!die)
        {
            newScale -= Time.deltaTime / 10;
            timeBar.transform.localScale = new Vector3(newScale, 1, 1);
            if (timeBar.transform.localScale.x < 0.01)
            {
                Player.Instance.Die();
                die = true;
            }
        }
        
    }
    public void ItemCollected()
    {
        newScale = 1;
    }
    public void Die()
    {
        leftButton.SetActive(false);
        rightButton.SetActive(false);
        text.SetActive(true);
        die = true;
        StartCoroutine(Buttons());
    }
    IEnumerator Buttons()
    {
        yield return new WaitForSeconds(2.3f);
        replayButton.SetActive(true);
        exitButton.SetActive(true);
    }
}
