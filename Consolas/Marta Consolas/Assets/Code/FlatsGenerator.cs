﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatsGenerator : MonoBehaviour
{
    static float verticalOffset = 3;

    int calls = 0;
    bool call;
    public Vector3 upVector;
    public Transform[] flatPool;

    public static FlatsGenerator Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        flatPool = new Transform[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
            flatPool[i] = transform.GetChild(i);

        StartGame();
        upVector = new Vector3(0, flatPool.Length * verticalOffset, 0);
    }

    private void StartGame()
    {
        for (int i = 1; i < flatPool.Length; i++)
        {
            flatPool[i].GetComponent<Flat>().Pattern(flatPool[i - 1].GetComponent<Flat>());
        }
    }
    public void PlatformHorizontalMovement(Transform platform)
    {
        int direction = flatPool[0].GetComponent<Flat>().HorizontalMove(platform);
 
        for(int i = 0; i< flatPool.Length; i++)
        {
            flatPool[i].GetComponent<Flat>().HorizontalMovement(direction);
        }
    }

    public void PlatformVerticalMovement()
    {
        calls++;
        if (calls > 1)
        {
            Up();
        }
    }

    private void Up()
    {
        Transform flat = flatPool[0];

        flat.position += upVector;

        flat.GetComponent<Flat>().Pattern(flatPool[flatPool.Length-1].GetComponent<Flat>());
        
        ReOrder();
    }

    private void ReOrder()
    {
        Transform[] temporaryArray = new Transform[flatPool.Length];
        int newPos;

        for (int i = 0; i < flatPool.Length; i++)
        {
            if (i == 0)
                newPos = flatPool.Length - 1;
            else
                newPos = (i - 1);

            temporaryArray[newPos] = flatPool[i];
        }

        for (int i = 0; i < temporaryArray.Length; i++)
        {
            flatPool[i] = temporaryArray[i];
        }

    }
}
