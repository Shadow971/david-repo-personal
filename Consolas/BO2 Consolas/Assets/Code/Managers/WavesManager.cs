﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WavesManager : MonoBehaviour
{
    #region Singleton
    public static WavesManager Instance;

    private void Awake()
    {
        Instance = this;        
    }
    #endregion Singleton

    #region Public Fields
    public Transform enemies;
    public int wave;
    public float waveReload;
    public GameObject ammoItem;
    public GameObject[] enemiesArray;
    public GameObject[] spawnPoints;

    #endregion

    #region Private Fields
    private Queue<GameObject> enemyPool = new Queue<GameObject>();

    private static int enemyBaseCount = 5;
    private int enemyCount; //How many you must kill for next wave
    private int enemyDead = 0; //How many enemies have you killed
    private int enemySpawned = 0; // Hoy many enemies are spawned now (Max 25)
    private int totalKills = 0;
    private bool newWave;
    #endregion

    
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < enemiesArray.Length; i++)
        {
            enemyPool.Enqueue(enemiesArray[i]);
        }

        UIManager.Instance.NewWave(wave + 1);
        NewWave();

    }


    void Update()
    {
        if (enemyCount == enemyDead && newWave)
        {
            newWave = false;
            StartCoroutine(NewWaveInSeconds());

        }
        if ( (enemySpawned < enemiesArray.Length) && ((enemySpawned + enemyDead) < enemyCount))
        {
            SpawnNewEnemy();
        }
    }

    private void SpawnNewEnemy()
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            GameObject objSpawn = spawnPoints[i].gameObject;
            if (objSpawn.activeInHierarchy)
            {
                Spawn currentSpawn = objSpawn.GetComponent<Spawn>();
                if (!currentSpawn.GetSpawning())
                {
                    currentSpawn.SpawnNewEnemy(enemyPool.Dequeue());
                    enemySpawned++;

                    return;
                }
            }
        }
    }

    private void NewWave()
    {
        wave++;
        totalKills += enemyDead;  
        enemyCount = enemyBaseCount * wave;

        enemyDead = 0;
        enemySpawned = 0;
        newWave = true;

        foreach(GameObject obj in enemyPool)
        {
            obj.GetComponent<Enemy>().NewWave(wave);
        }
    }

    public void EnemyDead(GameObject enemy)
    {
        enemyDead++;
        enemySpawned--;

        if(!ammoItem.activeSelf)
        {
            int rand = Random.Range(0, 5);
            if (rand == 0)
            {
                ammoItem.transform.position = enemy.transform.position;
                ammoItem.SetActive(true);
            }
        }

        enemyPool.Enqueue(enemy);
    }

    IEnumerator NewWaveInSeconds()
    {
        UIManager.Instance.NewWave(wave+1);
        yield return new WaitForSeconds(waveReload);
        NewWave();
    }
}
