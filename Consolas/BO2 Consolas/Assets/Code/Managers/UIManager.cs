﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Singleton
    public static UIManager Instance;

    private bool newWave = false;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    public GameObject[] hitsArray;
    private Queue<GameObject> hits = new Queue<GameObject>();
    public Transform texts;

    TextMeshProUGUI ammo;
    TextMeshProUGUI wave;
    TextMeshProUGUI money;

    Transform life;
    public GameObject menu;

    private void Start()
    {
        for (int i = 0; i < texts.childCount; i++)
        {
            hits.Enqueue(hitsArray[i]);
        }
        ShowMoney(0);
    }

    private void Update()
    {
        ShowLife();

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton9))
            OpenMenu();

    }
    public void OpenMenu()
    {
        if(menu.activeSelf)
        {
            menu.SetActive(false);
            Time.timeScale = 1;
            if (Inputs.Instance.windows)
                Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Time.timeScale = 0;
            menu.SetActive(true);
            if (Inputs.Instance.windows)
                Cursor.lockState = CursorLockMode.None;
        }
    }

    public void Hit(Vector3 pos, string value)
    {
        GameObject obj = hits.Dequeue();
        obj.SetActive(true);
        obj.GetComponent<UIHitText>().StartHit(pos, value);
        hits.Enqueue(obj);
    }
    public void ShowLife()
    {
        life.GetChild(1).localScale = new Vector3(1f - ((1f / 100) * Player.Instance.life), 1f, 1f);
    }

    public void ShowAmmo(int current, int clip, int max)
    {
        ammo.SetText(current + "  / " + max);
        ammo.transform.GetChild(1).localScale = new Vector3(1f - ((1f / clip) * current), 1f, 1f);
    }
    public void NewWave(int value)
    {
        wave.text = value.ToString();
        newWave = true;
        wave.gameObject.GetComponent<Animator>().SetBool("newWaveAnim",true);
        StartCoroutine(EndAnim());
    }

    public void ShowMoney(int _money)
    {
        money.text = ("" + _money);
    }

    public void SetAmmoAndWaveAndMoneyAndLife(TextMeshProUGUI ammo, TextMeshProUGUI wave, TextMeshProUGUI money, Transform image)
    {
        this.ammo = ammo;
        this.wave = wave;
        this.money = money;
        life = image;
    }

    IEnumerator EndAnim()
    {
        yield return new WaitForSeconds(3f);
        wave.gameObject.GetComponent<Animator>().SetBool("newWaveAnim", false);
    }

    public void Dead()
    {
        if (Inputs.Instance.windows)
            Cursor.lockState = CursorLockMode.None;
        wave.text = "you are dead";
        wave.gameObject.GetComponent<Animator>().SetBool("newWaveAnim", true);
        StartCoroutine(Die());
    }
    IEnumerator Die()
    {
        yield return new WaitForSeconds(3f);
        wave.gameObject.GetComponent<Animator>().SetBool("newWaveAnim", false);
        menu.SetActive(true);
    }
}
