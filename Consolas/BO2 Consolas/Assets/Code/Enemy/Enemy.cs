﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    #region Public Field
    public NavMeshAgent navMesh;
    public float attackReload;
    public static float lifeBasePoints = 20;
    #endregion

    #region Private Field
    private float life = 100;
    private float damage = 10;
    private bool canAttack = true;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        navMesh.SetDestination(Player.Instance.gameObject.transform.position);

        if(life <=0)
        {
            Dead();
        }
    }

    public void DoDamage(float damage)
    {
        life -= damage;
        Player.Instance.AddMoney(10);
    }

    private void Dead()
    {
        Player.Instance.AddMoney(10);
        life = 100;
        canAttack = true;
        WavesManager.Instance.EnemyDead(gameObject);
        gameObject.SetActive(false);
    }

    public void NewWave(int wave)
    {
        life = lifeBasePoints * wave;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag.Equals("Player") && canAttack)
        {
            canAttack = false;
            Player.Instance.DoDamage(damage);
            StartCoroutine(CanAttack());
        }
    }

    IEnumerator CanAttack()
    {
        yield return new WaitForSeconds(attackReload);
        canAttack = true;
    }
}
