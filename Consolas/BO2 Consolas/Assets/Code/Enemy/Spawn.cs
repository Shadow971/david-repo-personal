﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    private bool spawning = false;
    public float reloadTime;

    public void SpawnNewEnemy(GameObject enemy)
    {
        spawning = true;
        enemy.transform.position = transform.position;
        enemy.SetActive(true);
        StartCoroutine(Timer());
    }
    public bool GetSpawning()
    {
        return spawning;
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(reloadTime);
        spawning = false;
    }

}
