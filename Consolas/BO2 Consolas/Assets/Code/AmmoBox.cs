﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AmmoBox : MonoBehaviour
{
    public int price;
    private TextMeshPro text;
    private bool onReload = true;
    private float reload;
    public float baseReload;
    // Start is called before the first frame update
    void Start()
    {
        reload = baseReload;
        text = transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>();
        text.text = "" + price;
    }

    private void Update()
    {
        if(onReload)
        {
            if (reload <= 0)
            {
                onReload = false;
                reload = baseReload;
            }

            reload -= Time.deltaTime;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            text.gameObject.SetActive(true);
            text.transform.forward = Player.Instance.transform.forward;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        text.transform.forward = Player.Instance.transform.forward;
        if (!onReload)
        {
            text.text = "" + price;
            if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.Joystick1Button1) || Inputs.Instance.buy)
            {
                Player ply = Player.Instance;

                if (price <= ply.money)
                {
                    ply.money -= price;
                    Player.Instance.weapon.AmmoItem();
                    UIManager.Instance.ShowMoney(ply.money);
                    onReload = true;
                }
            }
        }
        else
        {
            text.text = (int)reload + "s";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        text.gameObject.SetActive(false);
    }
}
