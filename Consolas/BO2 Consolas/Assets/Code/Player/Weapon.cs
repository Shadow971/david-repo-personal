﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    #region Public Fields
    public GameObject mainCamera;
    public float damageBullet;
    public int clipAmmo;
    public int totalAmmo;

    public float reloadTime;
    #endregion

    #region Private Fields
    public float shootingTime;
    private int ammo;
    private int currentTotalAmmo;

    private bool canShoot;
    private bool lastAimed;
    private bool aimed;
    private bool isReloading = false;
    #endregion

    private void Start()
    {
        canShoot = true;
        ammo = clipAmmo;
        currentTotalAmmo = totalAmmo; 
        UIManager.Instance.ShowAmmo(ammo, clipAmmo, currentTotalAmmo);
        
    }

    private void Update()
    {
        if(!UIManager.Instance.menu.activeSelf)
        {
            if (Inputs.Instance.shoot && canShoot && ammo > 0 && !isReloading)
            {
                Shoot();
            }

            if (Inputs.Instance.reload && !isReloading && currentTotalAmmo > 0)
            {
                Reload();
            }
        }
    }

    public void AmmoItem()
    {
        currentTotalAmmo = totalAmmo;
        UIManager.Instance.ShowAmmo(ammo, clipAmmo, currentTotalAmmo);
    }

    private void Shoot()
    {
        canShoot = false;
        ammo--;
        
        RaycastHit hit;
        if (Physics.Raycast(mainCamera.transform.position, transform.forward, out hit, 100))
        {
            Enemy enemy = hit.transform.gameObject.GetComponent<Enemy>();
            if (hit.transform.CompareTag("EnemyBody"))
            {
                enemy.DoDamage(damageBullet);
                UIManager.Instance.Hit(hit.point, damageBullet.ToString());
            }
        }

        UIManager.Instance.ShowAmmo(ammo, clipAmmo, currentTotalAmmo);

        if (ammo > 0)
            StartCoroutine(ShootingTime());
        else
            StartCoroutine(ReloadTime());
    }

    private void Reload()
    {
        isReloading = true;
        Inputs.Instance.reload= false;
        StartCoroutine(ReloadTime());
    }

    IEnumerator ReloadTime()
    {
        yield return new WaitForSeconds(reloadTime);

        if (currentTotalAmmo > clipAmmo)
        {
            currentTotalAmmo -= (clipAmmo - ammo);
            ammo = clipAmmo;
        }
        else
        {
            ammo = currentTotalAmmo;
            currentTotalAmmo = 0;

        }
        
        canShoot = true;
        isReloading = false;
        UIManager.Instance.ShowAmmo(ammo,clipAmmo, currentTotalAmmo);
        Inputs.Instance.reload = false;
    }

    IEnumerator ShootingTime()
    {
        yield return new WaitForSeconds(shootingTime);
        canShoot = true;
    }
}
