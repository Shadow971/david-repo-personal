﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Singleton
    public static Player Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    #region Public Fields
    public Weapon weapon;
    public int money = 0;
    #endregion

    public float life = 100;
    private Vector3 offset;

    public float speed = 6f;
    public float camSpeed = 60f;
    
    private float dt;

    public bool dead = false;
    Vector3 deadPos;
    Vector3 deadRotation;

    private void Start()
    {
        weapon = transform.GetChild(0).GetChild(1).GetComponent<Weapon>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            dt = Time.deltaTime;
            Movement();
            if (life <= 0)
            {
                Die();
            }
        }
        else
        {
            transform.position = deadPos;
            transform.eulerAngles = deadRotation;
        }
    }

    private void Movement()
    {
        Vector2 mov = Inputs.Instance.movementAxis * speed * dt;
        Vector2 cam = Inputs.Instance.cameraAxis;

        
        //movement
        transform.Translate(new Vector3 (mov.x , 0, mov.y));

        //rotation
        offset.x += cam.x * camSpeed * dt;
        offset.y += cam.y * camSpeed * dt;

        offset.x = Mathf.Clamp(offset.x, -80, 80);

        transform.eulerAngles = new Vector3(0, offset.y, 0); 
        transform.GetChild(0).localEulerAngles = new Vector3 (offset.x, 0, 0);
    }

    public void DoDamage(float damage)
    {
        life -= damage;
    }

    private void Die()
    {
        UIManager.Instance.Dead();
        dead = true;
        deadPos = transform.position;
        deadRotation = transform.rotation.eulerAngles;

    }

    public void AddMoney(int _money)
    {
        money += _money;
        UIManager.Instance.ShowMoney(money);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("AmmoItem"))
        {
            weapon.AmmoItem();
            other.gameObject.SetActive(false);
        }
    }
}
