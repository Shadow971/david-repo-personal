﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Inputs : MonoBehaviour
{
    public Vector2 movementAxis;
    public Vector2 cameraAxis;
    public bool shoot;
    public bool reload;

    public GameObject canvasWindows;
    public GameObject canvasAndroid;

    public Joystick movementtJoyStick;
    public Joystick cameraJoyStick;

    public bool windows = false;
    public bool buy  = false;

    #region Singleton
    public static Inputs Instance;

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    private void Start()
    {
        Time.timeScale = 1;
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            UIManager.Instance.SetAmmoAndWaveAndMoneyAndLife(canvasWindows.transform.GetChild(0).GetComponent<TextMeshProUGUI>(), canvasWindows.transform.GetChild(1).GetComponent<TextMeshProUGUI>(), canvasWindows.transform.GetChild(2).GetComponent<TextMeshProUGUI>(), canvasWindows.transform.GetChild(3));
            canvasWindows.SetActive(true);
            windows = true;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            UIManager.Instance.SetAmmoAndWaveAndMoneyAndLife(canvasAndroid.transform.GetChild(0).GetComponent<TextMeshProUGUI>(), canvasAndroid.transform.GetChild(1).GetComponent<TextMeshProUGUI>(), canvasAndroid.transform.GetChild(2).GetComponent<TextMeshProUGUI>(), canvasAndroid.transform.GetChild(3));
            canvasAndroid.SetActive(true);
            windows = false;
        }

    }
    // Update is called once per frame
    private void Update()
    {
        if(windows)
        {
            InputMovementWindows();
            AttackButtonWindows();
            ReloadButtonWindows();
        }
        else
        {
            InputMovementAndroid();
        }
    }
    #region Windows
   
    private void InputMovementWindows()
    {
        movementAxis.x = Input.GetAxis("Horizontal");
        movementAxis.y = Input.GetAxis("Vertical");

        cameraAxis.y = Input.GetAxis("Mouse X");
        cameraAxis.x = -Input.GetAxis("Mouse Y");
    }
    private void AttackButtonWindows()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Joystick1Button5))
        {
            shoot = true;
        }
        if (Input.GetKeyUp(KeyCode.Mouse0) || Input.GetKeyUp(KeyCode.Joystick1Button5))
        {
            shoot = false;
        }
    }

    private void ReloadButtonWindows()
    {
        if (Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            reload = true;
        }
    }

    #endregion

    #region Android
    private void InputMovementAndroid()
    {
        movementAxis = movementtJoyStick.InputDir;

        cameraAxis.x = -cameraJoyStick.InputDir.y;
        cameraAxis.y = cameraJoyStick.InputDir.x;
    }
    public void AttackButtonAndroid()
    {
        shoot = true;
        StartCoroutine(EndAttack());
    }

    public void ReloadButtonAndroid()
    {
        reload = true;
    }

    public void Pause()
    {
       Time.timeScale = 0;
    }

    public void Buy()
    {
        buy = true;
        StartCoroutine(EndBuy());
    }

    IEnumerator EndAttack()
    {
        yield return new WaitForSeconds(0.5f);
        shoot = false;
    }
    IEnumerator EndBuy()
    {
        yield return new WaitForSeconds(0.5f);
        buy = false;
    }
    #endregion
}
