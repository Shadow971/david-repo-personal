﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIHitText : MonoBehaviour
{
    public int speed;
    public float deadTimer;


    // Update is called once per frame
    void Update()
    {
        transform.forward = Player.Instance.gameObject.transform.forward;
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
    
    public void StartHit(Vector3 pos, string txt)
    {
        transform.position = pos;
        transform.forward = Player.Instance.gameObject.transform.forward;
        transform.GetChild(0).GetComponent<TextMeshPro>().text = txt;
        StartCoroutine(EndHit());
    }

    IEnumerator EndHit()
    {
        yield return new WaitForSeconds(deadTimer);

        gameObject.SetActive(false);
    }
}
