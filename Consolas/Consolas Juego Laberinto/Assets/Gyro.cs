﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gyro : MonoBehaviour
{
    public GameObject cube;
    public Text txt;
    // Start is called before the first frame update
    void Start()
    {
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = "" + Input.gyro.attitude;
        GyroscopeInput();
    }
    void GyroscopeInput()
    {
        cube.transform.rotation = GyroToUnity(Input.gyro.attitude);
        cube.transform.Rotate(0f, 0f, 180f);
    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }
} 